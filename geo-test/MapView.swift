//
//  MapView.swift
//  geo-test
//
//  Created by Anish Khale on 2/14/20.
//  Copyright © 2020 First Performance Global. All rights reserved.
//

import SwiftUI
import MapKit
import FPGLocation

struct ContentView: View {
  @ObservedObject var annotation: Annotation
  var locationManager: LocationManager
  @State private var clientUserId: String = UserDefaults.standard.string(forKey: "clientUserId") ?? ""

  init(annotation: Annotation, locationManager: LocationManager) {
    self.annotation = annotation
    self.locationManager = locationManager
  }

  var body: some View {
    ZStack(alignment: .top) {
      MapView(annotation: annotation.location)
      ZStack(alignment: .trailing) {
        TextField("Enter clientUserId", text: $clientUserId)
          .padding(.horizontal)
          .textFieldStyle(RoundedBorderTextFieldStyle())
        Button(action: {
          UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)
          self.locationManager.clientUserId = self.clientUserId
          UserDefaults.standard.set(self.clientUserId, forKey: "clientUserId")
        }, label: {
          Text("Save")
        }).offset(x: -30, y: 0)
      }.offset(x: 0, y: 80)
    }
  }
}

class Annotation: ObservableObject {
  @Published var location = MKPointAnnotation()

  func append(_ location: MKPointAnnotation) {
    self.location = location

    print("location: \(self.location.coordinate.latitude), \(self.location.coordinate.longitude)")
  }
}

struct MapView: UIViewRepresentable {
  var annotation: MKPointAnnotation

  func makeUIView(context: Context) -> MKMapView {
    MKMapView(frame: .zero)
  }

  func updateUIView(_ view: MKMapView, context: Context) {
    view.addAnnotation(annotation)

    print("Annotation: \(annotation.coordinate.latitude), \(annotation.coordinate.longitude)")
  }
}
