//
//  AppDelegate.swift
//  geo-test
//
//  Created by Anish Khale on 2/13/20.
//  Copyright © 2020 First Performance Global. All rights reserved.
//

import UIKit
import FPGLocation
import UserNotifications
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, LocationManagerDelegate {

  public var locationManager: LocationManager!
  public var center: UNUserNotificationCenter!
  public var annotation: Annotation!
  let defaults = UserDefaults.standard

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Initialize and setup local notifications if you wish to get notified on every location report
    center = UNUserNotificationCenter.current()
    center.requestAuthorization(options: [.alert, .sound]) { granted, error in }
    
    // Get an instance of the Annotation class to drop pins on the map view
    // if you wish to visualize where each location is recorded while the app is in the foreground or background.
    // Annotation class is defined in MapView.swift
    annotation = Annotation()

    // Initialize FPGLocation
    locationManager = LocationManager(baseURL: URL(string: "https://your-central.firstperformance.com/api/v1")!, token: "your-ios-auth-token")
    locationManager.delegate = self

    // Set the clientUserId for the logged in user
    // A clientUserId is an identifier in your system that uniquely identifies the user.
    // This is the same value that's passed in the addCardSecure API during card registration.
    locationManager.clientUserId = defaults.string(forKey: "clientUserId")

    // Customize FPGLocation parameters
    // Set the minimum amount of time interval between two location reports, in multiples of 1 minute (defaults to 2)
    locationManager.minimumUpdateInterval = 4 // 2 minutes

    // Set the horizontal distance in meters required to trigger a new event (defaults to 200.0)
    locationManager.distanceFilter = 200.0 // 200 meters
    
    // Start recording locations
    locationManager.start()
    
    return true
  }
  
  public func locationManager(_ manager: LocationManager, didRecord location: Location) {
    // Called when FPGLocation successfully records a location
    // Use this method to send local notifications and drop pins on the map view
    
    // Create and send local notification
    let content = UNMutableNotificationContent()
    content.title = "New Location"
    content.body = "\(location.latitude), \(location.longitude)"
    content.sound = .default

    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.5,
                                                    repeats: false)

    let notification = UNNotificationRequest(identifier: UUID().uuidString,
                                             content: content,
                                             trigger: trigger)

    center.add(notification, withCompletionHandler: nil)

    // Create a new annotation and add it to the map view
    let newLocation = MKPointAnnotation()
    newLocation.title = ""
    newLocation.coordinate = CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)

    self.annotation.append(newLocation)
  }
  
  public func locationManager(_ manager: LocationManager, didFailWith error: LocationManagerError) {
  }
  
  public func locationManager(_ manager: LocationManager, didChange state: LocationAuthorizationState) {
  }

  // MARK: UISceneSession Lifecycle

  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }

  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }
}
